# vagrant-redox-test

This is an attempt to use Gitlab-CI with Vagrant to run basic Redox-OS fetch and install instructions as described on their website. Long term goal is to create series of "does it still work" repositories for this and other open-source projects.